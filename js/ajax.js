// pointeur sur la position de l'article courant dans le catalogue
            var index = 0;
            // initialisation du catalogue
            var catalogue = [];

            function executerRequete(callback) {
                // on vérifie si le catalogue a déjà été chargé pour n'exécuter la requête AJAX
                // qu'une seule fois
                if (catalogue.length === 0) {
                    // on récupère un objet XMLHttpRequest
                    var xhr = getXMLHttpRequest();
                    // on réagit à l'événement onreadystatechange
                    xhr.onreadystatechange = function() {
                        // test du statut de retour de la requête AJAX
                        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
                            // on désérialise le catalogue et on le sauvegarde dans une variable
                            catalogue = JSON.parse(xhr.responseText);
                            // on lance la fonction de callback avec le catalogue récupéré
                            callback();
                        }
                    }
                    // la requête AJAX : lecture de data.json
                    xhr.open("GET", "data/data.json", true);
                    xhr.send();
                } else {
                    // on lance la fonction de callback avec le catalogue déjà récupéré précédemment
                    callback();
                }
            }

            function lireSuivant() {
                // connaitre le nombre d'articles dans le catalogue
                var longueur = catalogue.length;
                // manipulation du DOM pour afficher les caractéristiques de l'article
                document.getElementById("intitule").innerHTML = catalogue[index].intitule;
                document.getElementById("desc").innerHTML = catalogue[index].desc;
                if (index < longueur - 1) {
                    index++;
                }
            }
            
            function lirePrecedent() {
                document.getElementById("intitule").innerHTML = catalogue[index].intitule;
                document.getElementById("desc").innerHTML = catalogue[index].desc;
                if (index > 0) {
                    index--;
                }
            }

            // on initialise la lecture au premier élément
            executerRequete(lireSuivant);