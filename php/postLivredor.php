<html>

<head>
    <title>Mes recettes</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Dr Sugiyama'>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Alex Brush'>
    <link rel="stylesheet" href="../css/style.css">
</head>

<body>
    <!-- Header -->
    <header class="header flex">
        <img src="../img/logoSite.png" alt="">
        <h1>Mister Cuistot</h1>
    </header>
    <!-- Navigation Bar -->
    <nav class="navbar">
        <a href="../index.html">Accueil</a>
        <a href="../menus.html">Menus de la semaine</a>
        <a href="../livredor.html">Livre d'Or</a>
        <a href="../contact.html">Contact</a>
    </nav>
    <!-- Main -->
    <main>
        <section>
            <article class="bandeauBrun titre">
                <h1>Formulaire envoyé, à très bientôt !</h1>
            </article>
        </section>
    </main>
    <!-- Footer -->
    <nav class="navbar footer">
        <a href=""> © Mister Cuistot</a>
        <a href="">Mentions légales</a>
    </nav>
</body>

</html>

<?php

// On teste la connexion avec la base
try
{
    // On se connecte à MySQL
    $bdd = new PDO('mysql:host=localhost;dbname=bdd_mistercuistot;charset=utf8', 'root', 'sudosu');
}
catch(Exception $e)
{
    // En cas d'erreur, on affiche un message et on arrête tout
    die('Erreur : '.$e->getMessage());
    // Si tout va bien, on peut continuer
}

// Je déclare mes variables
$id = $_POST['id'];
$nom = $_POST['nom'];
$prenom = $_POST['prenom'];
$avis = $_POST['avis'];
$email = $_POST['email'];

// On écrit les valeurs du formulaire dans la table A CHANGER PAR :nom !!!!!!!!!!!!!!
$bdd->exec("INSERT INTO table_livredor (nom, prenom, avis, email) VALUES ('".$nom."', '".$prenom."', '".$avis."', '".$email."')");

// Envoi d'un mail au client ===========================================================================
// On vérifie les erreurs
ini_set( 'display_errors', 1 );
error_reporting( E_ALL );
// On envoi le mail avec les paramètres suivants :
$from = "testlafabriquenumerique@gmail.com";
$to = $email;
$subject = "Retour d'experience MisterCuistot";
$message = "Merci de votre retour, à très bientôt chez MisterCuistot !";
$headers = "Mister Cuistot" . $from;

mail($to,$subject,$message, $headers);

?>