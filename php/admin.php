<!DOCTYPE html>
<html>

<head>
    <title>Contacter MisterCuistot</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Dr Sugiyama'>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Alex Brush'>
    <link rel="stylesheet" href="../css/style.css">
    <script type="text/javascript" src="../phpmyadmin/js/jquery/jquery.min.js"></script>
    <!-- formulaire.js -->
    <script type="text/javascript" language="javascript" src="../js/formulaire.js"></script>
</head>

<body>
    <!-- Header -->
    <header class="header flex">
        <img src="../img/logoSite.png" alt="">
        <h1>Mister Cuistot</h1>
    </header>
    <!-- Navigation Bar -->
    <nav class="navbar">
        <a href="../index.html">Accueil</a>
        <a href="../menus.html">Menus de la semaine</a>
        <a href="../livredor.html">Livre d'Or</a>
        <a href="../contact.html">Contact</a>
    </nav>
    <!-- Main -->
    <main>
        <section class="flex">
            <article class="chef">
                <h1 class="titre">Données de contact</h1>
                <!-- Recupérer les données dans la base ------------------------------------------------------------ -->
                <?php
                    // On teste la connexion avec la base
                    try
                    {
                        // On se connecte à MySQL
                        $bdd = new PDO('mysql:host=localhost;dbname=bdd_mistercuistot;charset=utf8', 'root', 'sudosu');
                    }
                    catch(Exception $e)
                    {
                        // En cas d'erreur, on affiche un message et on arrête tout
                        die('Erreur : '.$e->getMessage());
                        // Si tout va bien, on peut continuer
                    }
                    // On récupère tout le contenu de la table
                    $reponse = $bdd->query('SELECT * FROM table_contact');
                    // On affiche chaque entrée une à une
                    while ($donnees = $reponse->fetch())
                    {
                        ?>
                            <!-- Recupérer les données dans la base ------------------------------------------------------------ -->
                        <p>
                            === Contact N°<?php echo $donnees['id']; ?> ===<br><br>
                            Nom : <?php echo $donnees['nom']; ?><br />
                            Prénom : <?php echo $donnees['prenom']; ?><br />
                            Téléphone : <?php echo $donnees['tel']; ?><br />
                            Portable : <?php echo $donnees['mobile']; ?><br />
                            Courriel : <?php echo $donnees['email']; ?><br />
                            Adresse : <?php echo $donnees['adresse']; ?><br><br><br>
                        </p>
                        <?php
                    }
                    $reponse->closeCursor(); // Termine le traitement de la requête
                ?>
            </article>

            <article class="bandeauClair">
                <h1 class="titre">Données de livre d'or</h1>
                <?php
                    // On récupère tout le contenu de la table
                    $reponse = $bdd->query('SELECT * FROM table_livredor');
                    // On affiche chaque entrée une à une
                    while ($donnees = $reponse->fetch())
                    {
                        ?>
                        <p>
                            === Commentaire N°<?php echo $donnees['id']; ?> ===<br><br>
                            Nom : <?php echo $donnees['nom']; ?><br />
                            Prénom : <?php echo $donnees['prenom']; ?><br />
                            Commentaire : <?php echo $donnees['avis']; ?><br />
                            Courriel : <?php echo $donnees['email']; ?><br><br><br>
                        </p>
                        <?php
                    }
                    $reponse->closeCursor(); // Termine le traitement de la requête
                ?>
            </article>

            <!-- Zone de test ------------------------------------------------------------------------>

            <article class="bandeauClair">
                <h1 class="titre">Données de livre d'or modifiables</h1>
                <?php
                    // On récupère tout le contenu de la table
                    $reponse = $bdd->query('SELECT * FROM table_livredor');
                    // On affiche chaque entrée une à une
                    while ($donnees = $reponse->fetch())
                    {
                        ?>
                        === Commentaire N°<?php echo $donnees['id']; ?> ===<br><br>
                        <form method="post" action="updateLivredor.php">
                        <input type="text" id="nom" name="nom" value="<?php echo $donnees['nom'];?>" required placeholder="Votre nom">
                        <input type="text" id="prenom" name="prenom" value="<?php echo $donnees['prenom'];?>" required placeholder="Votre prénom">
                        <textarea type="text" id="avis" name="avis" value="<?php echo $donnees['avis'];?>" rows="4" required placeholder="Votre commentaire"></textarea>
                        <input type="email" id="email" name="email" value="<?php echo $donnees['email'];?>" required placeholder="Votre mail">
                        <button type="submit">Envoyer </button>
                        <button type="reset">Annuler</button>
                        </form>
                        <?php
                    }
                    $reponse->closeCursor(); // Termine le traitement de la requête
                ?>
            </article>
        </section>
    </main>
    <!-- Footer -->
    <div class="navbar footer">
        <a href=""> © Mister Cuistot</a>
        <a href="">Mentions légales</a>
    </div>
</body>

</html>