<html>

<head>
    <title>Mes recettes</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Dr Sugiyama'>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Alex Brush'>
    <link rel="stylesheet" href="../css/style.css">
</head>

<body>
    <!-- Header -->
    <header class="header flex">
        <img src="../img/logoSite.png" alt="">
        <h1>Mister Cuistot</h1>
    </header>
    <!-- Navigation Bar -->
    <nav class="navbar">
        <a href="../index.html">Accueil</a>
        <a href="../menus.html">Menus de la semaine</a>
        <a href="../livredor.html">Livre d'Or</a>
        <a href="../contact.html">Contact</a>
    </nav>
    <!-- Main -->
    <main>
        <section>
            <article class="bandeauBrun titre">
                <h1>Formulaire envoyé, à très bientôt !</h1>
            </article>
        </section>
    </main>
    <!-- Footer -->
    <div class="navbar footer">
        <a href=""> © Mister Cuistot</a>
        <a href="">Mentions légales</a>
    </div>
</body>

</html>

<?php

// On teste et on se connecte avec la base ===========================================================================
try
{
    // On se connecte à MySQL
    $bdd = new PDO('mysql:host=localhost;dbname=bdd_mistercuistot;charset=utf8', 'root', 'sudosu');
}
catch(Exception $e)
{
    // En cas d'erreur, on affiche un message et on arrête tout
    die('Erreur : '.$e->getMessage());
    // Si tout va bien, on peut continuer
}

// Je déclare mes variables ===========================================================================
$nom = $_POST['nom'];
$prenom = $_POST['prenom'];
$tel = $_POST['tel'];
$mobile = $_POST['mobile'];
$email = $_POST['email'];
$adresse = $_POST['adresse'];

// On écrit les valeurs du formulaire dans la table ===========================================================================
$bdd->exec("INSERT INTO table_contact (nom, prenom, tel, mobile, email, adresse) VALUES ('".$nom."', '".$prenom."', '".$tel."', '".$mobile."', '".$email."', '".$adresse."')");

// Envoi d'un mail au client ===========================================================================
// On vérifie les erreurs
ini_set( 'display_errors', 1 );
error_reporting( E_ALL );
// On envoi le mail avec les paramètres suivants :
$from = "testlafabriquenumerique@gmail.com";
$to = $email;
$subject = "Retour d'experience MisterCuistot";
$message = "Merci de votre retour, à très bientôt chez MisterCuistot !";
$headers = "Mister Cuistot" . $from;

mail($to,$subject,$message, $headers);




?>
